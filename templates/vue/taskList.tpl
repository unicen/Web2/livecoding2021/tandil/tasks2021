{literal}
<div id="app">
    <h1>{{ titulo }}</h1>
    <h2>{{ subtitulo }}</h2>

    <ul id="lista-tareas" class="list-group">
        <li v-for="tarea in tareas" class="list-group-item">
            {{tarea.titulo}} | {{tarea.descripcion}}

            <a v-if="tarea.finalizada == 0" class="btn btn-success">Done</a>
        </li>
    </ul>
</div>
{/literal}