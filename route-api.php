<?php

require_once 'libs/Router.php';
require_once 'Controller/ApiTaskController.php';
require_once 'Controller/ApiUserController.php';

// crea el router
$router = new Router();

// define la tabla de ruteo
$router->addRoute('tareas', 'GET', 'ApiTaskController', 'obtenerTareas');
$router->addRoute('tareas/:ID', 'GET', 'ApiTaskController', 'obtenerTarea');
$router->addRoute('tareas/:ID', 'DELETE', 'ApiTaskController', 'eliminarTarea');
$router->addRoute('tareas', 'POST', 'ApiTaskController', 'insertarTarea');
$router->addRoute('tareas/:ID', 'PUT', 'ApiTaskController', 'actualizarTarea');

$router->addRoute('users/token', 'GET', 'ApiUserController', 'obtenerToken');
$router->addRoute('users/:ID', 'GET', 'ApiUserController', 'obtenerUsuario');

// rutea
$router->route($_GET["resource"], $_SERVER['REQUEST_METHOD']);