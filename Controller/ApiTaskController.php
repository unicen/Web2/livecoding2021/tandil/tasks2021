<?php
require_once "./Model/TaskModel.php";
require_once "./View/ApiView.php";

class ApiTaskController{

    private $model;
    private $view;

    function __construct(){
        $this->model = new TaskModel();
        $this->view = new ApiView();
    }

    function obtenerTareas(){
        $tareas = $this->model->getTasks();
        return $this->view->response($tareas, 200);
    }

    function obtenerTarea($params = null) {
        $idTarea = $params[":ID"];
        $tarea = $this->model->getTask($idTarea);
        if ($tarea) {
            return $this->view->response($tarea, 200);
        } else {
            return $this->view->response("La tarea con el id=$idTarea no existe", 404);
        }
    }

    function eliminarTarea($params = null) {
        $idTarea = $params[":ID"];
        $tarea = $this->model->getTask($idTarea);

        if ($tarea) {
            $this->model->deleteTaskFromDB($idTarea);
            return $this->view->response("La tarea con el id=$idTarea fue borrada", 200);
        } else {
            return $this->view->response("La tarea con el id=$idTarea no existe", 404);
        }
    }

    function insertarTarea($params = null) {
        // obtengo el body del request (json)
        $body = $this->getBody();

        // TODO: VALIDACIONES -> 400 (Bad Request)

        $id = $this->model->insertTask($body->titulo, $body->descripcion, $body->prioridad, false);
        if ($id != 0) {
            $this->view->response("La tarea se insertó con el id=$id", 200);
        } else {
            $this->view->response("La tarea no se pudo insertar", 500);
        }
    }

    function actualizarTarea($params = null) {
        $idTarea = $params[':ID'];
        $body = $this->getBody();
        
        // TODO: VALIDACIONES -> 400 (Bad Request)

        $tarea = $this->model->getTask($idTarea);

        if ($tarea) {
            $this->model->update($idTarea, $body->titulo, $body->descripcion, $body->prioridad, $body->finalizada);
            $this->view->response("La tarea se actualizó correctamente", 200);
        } else {
            return $this->view->response("La tarea con el id=$idTarea no existe", 404);
        }
    }

    /**
     * Devuelve el body del request
     */
    private function getBody() {
        $bodyString = file_get_contents("php://input");
        return json_decode($bodyString);
    }

}